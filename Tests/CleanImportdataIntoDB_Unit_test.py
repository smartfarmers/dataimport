import CleanImportDataIntoDB as testScript
import random
from unittest import TestCase
from unittest.mock import patch
from datetime import datetime

def test_generateActivityQuery():
    result = testScript.generateActivityQuery("Test_Table_Name")
    expect = ("CREATE TABLE Test_Table_Name "
        "(ACT_ID_FOUR_SEC BIGINT NOT NULL AUTO_INCREMENT,"
        "ACT_DATE_TIME DATETIME,"
        "COLLAR_LOC_ID BIGINT NOT NULL,"
        "ACT_STAT SMALLINT,"
        "ACT_REC SMALLINT,"
        "PRIMARY KEY (ACT_ID_FOUR_SEC ),"
        "FOREIGN KEY (COLLAR_LOC_ID) REFERENCES COLLAR_ALLOCATION(COLLAR_LOC_ID))")
    assert result == expect


def test_generateLocationQuery():
    result = testScript.generateLocationQuery("Test_Table_Name")
    expect = ("CREATE TABLE Test_Table_Name "
        "(LOC_ID_FIVE_MIN BIGINT NOT NULL AUTO_INCREMENT,"
        "LOC_DATE_TIME DATETIME,"
        "COLLAR_LOC_ID BIGINT NOT NULL,"
        "LOC_LAT FLOAT,"
        "LOC_LON FLOAT,"
        "LOC_REC MEDIUMINT,"
        "PRIMARY KEY (LOC_ID_FIVE_MIN ),"
        "FOREIGN KEY (COLLAR_LOC_ID) REFERENCES COLLAR_ALLOCATION(COLLAR_LOC_ID))")
    assert result == expect

@patch('CleanImportDataIntoDB.checkTableExists', return_value=True)
def test_addLocationDataWhenTableDoesExist(self):
    today = datetime.today()
    date = today.strftime("%b%Y")
    result = testScript.addLocationData('0-123456', "TestCursor", "TestCNX")
    expect = ( "INSERT INTO location_five_mins_jun2020_0_3268160" #TODO ( "INSERT INTO LOCATION_FIVE_MINS_" + date + "_0_123456 " # Change result to match manual table name
            "(LOC_DATE_TIME, COLLAR_LOC_ID, LOC_LAT, LOC_LON, LOC_REC) "
            "VALUES (%(LOC_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(LOC_LAT)s, %(LOC_LON)s, %(LOC_REC)s)")
    assert result == expect

@patch('CleanImportDataIntoDB.checkTableExists', return_value=False)
@patch('CleanImportDataIntoDB.createActivityTable')
def test_addLocationDataWhenTableDoesNotExist(self, *args):
    today = datetime.today()
    date = today.strftime("%b%Y")
    result = testScript.addLocationData('0-123456', "TestCursor", "TestCNX")
    expect = ( "INSERT INTO location_five_mins_jun2020_0_3268160" # TODO ( "INSERT INTO LOCATION_FIVE_MINS_" + date + "_0_123456 "
            "(LOC_DATE_TIME, COLLAR_LOC_ID, LOC_LAT, LOC_LON, LOC_REC) "
            "VALUES (%(LOC_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(LOC_LAT)s, %(LOC_LON)s, %(LOC_REC)s)")
    assert result == expect

@patch('CleanImportDataIntoDB.checkTableExists', return_value=True)
def test_addActivityDataWhenTableDoesExist(self):
    today = datetime.today()
    date = today.strftime("%b%Y")
    result = testScript.addActivityData('0-123456', "TestCursor", "TestCNX")
    expect = ( "INSERT INTO activity_four_secs_jun2020_0_3268160" #TODO ( "INSERT INTO ACTIVITY_FOUR_SECS_" + date + "_0_123456 "
                "(ACT_DATE_TIME, COLLAR_LOC_ID, ACT_STAT, ACT_REC) "
                "VALUES (%(ACT_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(ACT_STAT)s, %(ACT_REC)s)")
    assert result == expect

@patch('CleanImportDataIntoDB.checkTableExists', return_value=False)
@patch('CleanImportDataIntoDB.createActivityTable')
def test_addActivityDataWhenTableDoesNotExist(self, *args):
    today = datetime.today()
    date = today.strftime("%b%Y")
    result = testScript.addActivityData('0-123456', "TestCursor", "TestCNX")
    expect = ( "INSERT INTO activity_four_secs_jun2020_0_3268160" #TODO ( "INSERT INTO ACTIVITY_FOUR_SECS_" + date + "_0_123456 "
                "(ACT_DATE_TIME, COLLAR_LOC_ID, ACT_STAT, ACT_REC) "
                "VALUES (%(ACT_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(ACT_STAT)s, %(ACT_REC)s)")
    assert result == expect


def test_processActivityWithAsterisk():
    test_value = "***"
    result = testScript.processActivity(test_value)
    expect = None
    assert result == expect

def test_processACtivityWithoutAsterisk():
    test_value = "Test Recording"
    result = testScript.processActivity(test_value)
    expect = test_value
    assert result == expect

def test_getLatLongData():
    result = testScript.getLatLongData("1.222 1.22", "1.222 1.22", "1.222 1.22")
    expect = [['1.222', '1.22'], ['1.222', '1.22'], ['1.222', '1.22']]
    assert result == expect

@patch('CleanImportDataIntoDB.getFileIdByFileName', return_value=["123", 1])
def test_checkFileExistsWhenFileExists(self):
    result = testScript.checkFileExists("testfile", "test_path", "test_cursor", "test_cnx", "test_collar_id")
    expect = "123"
    assert result == expect

def test_processLocationLatLongWithoutError():
    test_data = "1.7777 -1.2222"
    result, result_error = testScript.processLocationLatLong(test_data, [])
    expect = ["1.7777", "-1.2222"]
    expect_error = []
    assert result == expect
    assert result_error == expect_error

def test_processLocationLatLongWithAsteriskError():
    test_data = "*"
    test_error_logging = {
            "* in location": 0,
            "null location": 0,
            "---- in location": 0
        }
    result, result_error = testScript.processLocationLatLong(test_data, test_error_logging)
    expect = [None, None]
    expect_error_num = 1
    assert result == expect
    assert result_error["* in location"] == expect_error_num

def test_processLocationLatLongWithDashError():
    test_data = "---------"
    test_error_logging = {
            "* in location": 0,
            "null location": 0,
            "---- in location": 0
        }
    result, result_error = testScript.processLocationLatLong(test_data, test_error_logging)
    expect = [None, None]
    expect_error_num = 1
    assert result == expect
    assert result_error["---- in location"] == expect_error_num

def test_processLocationLatLongWithNullError():
    test_data = ""
    test_error_logging = {
            "* in location": 0,
            "null location": 0,
            "---- in location": 0
        }
    result, result_error = testScript.processLocationLatLong(test_data, test_error_logging)
    expect = [None, None]
    expect_error_num = 1
    assert result == expect
    assert result_error["null location"] == expect_error_num


def test_processRecordRowWithRecord():
    test_data = ["date","latLong","Record 1"]
    result = testScript.processRecordRow(test_data)
    expect = "1"
    assert result == expect

def test_processRecordRowWithoutRecord():
    test_data = ["date","latLong"]
    result = testScript.processRecordRow(test_data)
    expect = None
    assert result == expect

@patch('random.randint', return_value=100)
@patch('mysql.connector')
def test_generateTempCowId(self, mock_connection):
    result = testScript.generateTempCowId("01234", mock_connection, mock_connection)
    expect = "temp_100"
    assert result == expect

@patch('CleanImportDataIntoDB.checkCollarAllocationIdByCowId', return_value="collarAllocation")
def test_getCollarAllocation(self):
    result = testScript.getCollarAllocation("1234", "M12", "Test_cnx", "test_cursor")
    expect = "collarAllocation"
    assert result == expect

@patch('CleanImportDataIntoDB.checkCollarAllocationIdByCowId', return_value="")
@patch('CleanImportDataIntoDB.generateCollarAllocation', return_value="NewCollarAllocation")
def test_getCollarAllocationWhenNoneExists(self, *args):
    result = testScript.getCollarAllocation("1234", "M12", "Test_cnx", "test_cursor")
    expect = "NewCollarAllocation"
    assert result == expect
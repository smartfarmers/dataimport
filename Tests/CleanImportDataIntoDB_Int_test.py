import CleanImportDataIntoDB as testScript
import pytest_helper_functions as helperFunctions
from datetime import datetime
from zipfile import ZipFile
import TestPythonSQL as test_sql
import mysql.connector
import pytest
import os
from unittest.mock import patch


# TODO Add exception testing

# SQL CONSTANTS
SQL_SERVICE_USER = 'root'
SQL_SERVICE_PASSWORD = ''
SQL_HOST = '127.0.0.1'
SQL_DATABASE = 'smartFarm_test'

def setupMySQLConnection(user, password, host, database):
    cnx = mysql.connector.connect(user=user, password=password,host=host,database=database)
    return cnx

def setupDatabaseConnection():
    cnx = setupMySQLConnection(SQL_SERVICE_USER, SQL_SERVICE_PASSWORD, SQL_HOST, SQL_DATABASE)
    db_cursor = cnx.cursor()
    return db_cursor, cnx

def createTestDatabase():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.init_SQL_Test_DB, multi=True)
    cnx.commit()

def test_testSqlDatabaseCreation():
    createTestDatabase()

def test_checkTableExistsWhenTableDoesNotExist():
    db_cursor, cnx = setupDatabaseConnection()
    test_tableName = "test"
    result = testScript.checkTableExists(test_tableName, db_cursor)
    expect = None
    assert result == expect

def test_checkTableExistsWhenTableDoesExist():
    db_cursor, cnx = setupDatabaseConnection()
    test_tableName = "cow"
    result = testScript.checkTableExists(test_tableName, db_cursor)
    expect = test_tableName
    assert result == expect

def test_createActivityTable():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.drop_act_table)
    cnx.commit()
    test_table_name = "activity_four_secs_jun2020_0_3268160"
    testScript.createActivityTable(test_table_name, db_cursor, cnx)
    result = testScript.checkTableExists(test_table_name, db_cursor)
    expect = test_table_name
    assert result == expect

def test_createLocationTable():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.drop_loc_table)
    cnx.commit()
    test_table_name = "location_five_mins_jun2020_0_3268160"
    testScript.createLocationTable(test_table_name, db_cursor, cnx)
    result = testScript.checkTableExists(test_table_name, db_cursor)
    expect = test_table_name
    assert result == expect

def test_getCowIdByCollarId():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.drop_loc_table)
    cnx.commit()
    test_collar_id = "123"
    test_cow_id = "temp"
    result = testScript.getCowIdByCollarId(test_collar_id, db_cursor, cnx)
    assert (test_cow_id in result) == True
    db_cursor.execute(test_sql.del_cow, { 'cow_id' : result })
    cnx.commit()

def test_getCollarIdFromCollarNum():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.insert_test_collar)
    cnx.commit()
    test_collar_num = "s18"
    test_collar_id = "123"
    test_cow_id = "123"
    testScript.createCow(test_cow_id, cnx, db_cursor)
    result = testScript.getCollarIdFromCollarNum(test_collar_num, cnx, db_cursor)
    assert result == test_collar_id
    db_cursor.execute(test_sql.del_coll)
    cnx.commit()
    db_cursor.execute(test_sql.del_cow, { 'cow_id' : test_cow_id })
    cnx.commit()
    

# TODO create collar allocation
def test_getCollarAllocation():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.reset_coll_all_id)
    cnx.commit()
    db_cursor.execute(test_sql.insert_test_collar)
    cnx.commit()
    test_collar_id = "123"
    test_cow_id = "01"
    test_cow_id = "123"
    testScript.createCow(test_cow_id, cnx, db_cursor)
    result = testScript.getCollarAllocation(test_collar_id, test_cow_id, cnx, db_cursor)
    expect = 1
    assert result == expect
    db_cursor.execute(test_sql.del_coll_all, { 'collar_loc_id' : result })
    cnx.commit()
    db_cursor.execute(test_sql.del_coll)
    cnx.commit()
    db_cursor.execute(test_sql.del_cow, { 'cow_id' : test_cow_id })
    cnx.commit()


# TODO create test for importActivityCSV
# def test_importActivityCsv():
#     db_cursor, cnx = setupDatabaseConnection()
#     db_cursor.execute(test_sql.reset_coll_all_id)
#     cnx.commit()
#     db_cursor.execute(test_sql.insert_test_collar)
#     cnx.commit()
#     test_collar_id = "123"
#     test_cow_id = "01"
#     test_cow_id = "123"
#     testScript.createCow(test_cow_id, cnx, db_cursor)
#     test_coll_all = testScript.getCollarAllocation(test_collar_id, test_cow_id, cnx, db_cursor)
#     test_file_name = "activity4s-s18-1003102611.csv"
#     test_file_path = "./TestData/activity4s-s18-1003102611.csv"
#     test_collar_allocation_id = "1"
#     test_collar_id = "1234"
#     testScript.importActivityCsv(test_file_name, test_file_path, db_cursor, cnx, test_collar_allocation_id, test_collar_id)
#     cnx.commit()
#     # Cleanup
#     db_cursor.execute(test_sql.del_coll_all, { 'collar_loc_id' : test_coll_all })
#     cnx.commit()
#     db_cursor.execute(test_sql.del_coll)
#     cnx.commit()
#     db_cursor.execute(test_sql.del_cow, { 'cow_id' : test_cow_id })
#     cnx.commit()

def test_insertActivityDataExceptionNoCollarAlocationSetup():
    with pytest.raises(Exception):
        db_cursor, cnx = setupDatabaseConnection()
        test_data_time = "2020/03/03 13:21:53"
        test_activity = "1"
        test_record_id = "1"
        test_collar_id = "0_1234"
        test_collar_allocation_id = "123"
        testScript.insertActivityData(db_cursor, cnx, test_collar_allocation_id, test_data_time, test_activity, test_record_id, test_collar_id)

def test_checkFileExists():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.reset_file_id)
    cnx.commit()
    db_cursor.execute(test_sql.insert_test_collar)
    cnx.commit()
    test_file_id = 1
    test_file_name = "test_file_name"
    test_file_path = "./TestData/location5min-s18-1003102818.csv"
    test_collar_id = "123"
    result = testScript.checkFileExists(test_file_name, test_file_path, db_cursor, cnx, test_collar_id)
    assert result == test_file_id
    db_cursor.execute(test_sql.del_dile_meta)
    cnx.commit()
    db_cursor.execute(test_sql.del_coll)
    cnx.commit()

    

# TODO resolve bugs with create test for importLocationCSV
# def test_importLocationCsv():
#     db_cursor, cnx = setupDatabaseConnection()
#     db_cursor.execute(test_sql.reset_coll_all_id)
#     db_cursor.execute(test_sql.reset_file_id)
#     cnx.commit()
#     db_cursor.execute(test_sql.insert_test_collar)
#     cnx.commit()
#     test_collar_id = "123"
#     test_cow_id = "01"
#     test_cow_id = "123"
#     test_file_name = "location5min-s18-1003102818.csv"
#     test_file_path = "./TestData/location5min-s18-1003102818.csv"
#     test_file_id = testScript.checkFileExists(test_file_name, test_file_path, db_cursor, cnx, test_collar_id)
#     testScript.createCow(test_cow_id, cnx, db_cursor)
#     test_coll_all = testScript.getCollarAllocation(test_collar_id, test_cow_id, cnx, db_cursor)
#     test_collar_allocation_id = "1"
#     testScript.importLocationCsv(test_file_name, test_file_path, db_cursor, cnx, test_collar_allocation_id, test_collar_id, test_file_id)
#     cnx.commit()
#     # Cleanup
#     db_cursor.execute(test_sql.del_coll_all, { 'collar_loc_id' : test_coll_all })
#     cnx.commit()
#     db_cursor.execute(test_sql.del_coll)
#     cnx.commit()
#     db_cursor.execute(test_sql.del_cow, { 'cow_id' : test_cow_id })
#     cnx.commit()

def test_importDailtyCsv():
    db_cursor, cnx = setupDatabaseConnection()
    db_cursor.execute(test_sql.reset_coll_all_id)
    cnx.commit()
    db_cursor.execute(test_sql.insert_test_collar)
    cnx.commit()
    test_collar_id = "123"
    test_cow_id = "01"
    test_cow_id = "123"
    testScript.createCow(test_cow_id, cnx, db_cursor)
    test_coll_all = testScript.getCollarAllocation(test_collar_id, test_cow_id, cnx, db_cursor)
    test_file_name = "Daily-s18-1003102603.csv"
    test_file_path = "./TestData/Daily-s18-1003102603.csv"
    testScript.importDailyCsv(test_file_name, test_file_path, db_cursor, cnx, test_coll_all, test_collar_id)
    cnx.commit()
    # Cleanup
    db_cursor.execute(test_sql.del_loc_daily)
    db_cursor.execute(test_sql.del_act_daily)
    db_cursor.execute(test_sql.del_bat_daily)
    db_cursor.execute(test_sql.del_status_daily)
    db_cursor.execute(test_sql.del_temp_daily)
    cnx.commit()
    db_cursor.execute(test_sql.del_coll_all, { 'collar_loc_id' : test_coll_all })
    cnx.commit()
    db_cursor.execute(test_sql.del_coll)
    cnx.commit()
    db_cursor.execute(test_sql.del_cow, { 'cow_id' : test_cow_id })
    cnx.commit()
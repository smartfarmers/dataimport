# Contains functions used to assist in testing

import os
import shutil
from zipfile import ZipFile

def createTestFolder(folderName):
    path = os.getcwd()
    new_path = path + "/" + folderName
    os.mkdir(new_path)
    return new_path

def createTestZips(parentFolder):
    parentFolder = parentFolder + "/"
    test_folder1 = parentFolder + "testFolder1"
    test_folder2 = parentFolder + "testFolder2"
    os.mkdir(test_folder1)
    os.mkdir(test_folder2)
    test_zip1 = ZipFile('./testFolder/testZip1.zip', 'w')
    test_zip1.write("./testFolder/testFolder1")
    test_zip1.close()
    test_zip2 = ZipFile('./testFolder/testZip2.zip', 'w')
    test_zip2.write("./testFolder/testFolder2")
    test_zip2.close()
    os.rmdir(test_folder1)
    os.rmdir(test_folder2)


def removeTestFolder(parentFolder):
    shutil.rmtree(parentFolder)
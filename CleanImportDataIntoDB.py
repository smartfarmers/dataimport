import csv
import os
import sys
import mysql.connector
import random
import time
import pandas
from zipfile import ZipFile
from datetime import datetime

###############################################################
# SQL CONSTANTS
###############################################################
SQL_SERVICE_USER = 'root'
SQL_SERVICE_PASSWORD = 'P@ssword1'
SQL_HOST = '127.0.0.1'
SQL_DATABASE = 'smartFarm'

###############################################################
# SQL QUERIES
###############################################################
add_cow = ("INSERT INTO COW "
                "(cow_id, date_of_birth, date_of_entry, date_of_exit) "
                "VALUES (%s, %s, %s, %s)")

add_collar = ("INSERT INTO COLLAR "
                "(collar_id, collar_ser_no, collar_num, date_of_entry) "
                "VALUES (%s, %s, %s, %s)")

add_collar_allocation = ("INSERT INTO COLLAR_ALLOCATION "
                "(cow_id, collar_id, date_on, date_off) "
                "VALUES (%s, %s, %s, %s)")
                
add_daily_activity = ("INSERT INTO ACTIVITY_DAILY "
                "(act_date, collar_loc_id, act_rest, act_walk, act_graze, act_bout) "
                "VALUES (%s, %s, %s, %s, %s, %s)")

add_daily_location = ("INSERT INTO LOCATION_DAILY "
                "(loc_date, collar_loc_id, loc_1_lat, loc_1_long, loc_2_lat, loc_2_long, loc_3_lat, loc_3_long) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)")

add_battery = ("INSERT INTO BATTERY "
                "(batt_date, collar_loc_id, batt_min, batt_max, batt_00_00, batt_04_00, batt_08_00, batt_12_00, batt_16_00, batt_20_00) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")

add_status = ("INSERT INTO STATUS "
                "(stat_date, collar_loc_id, stat_sys) "
                "VALUES (%s, %s, %s)")

add_temperature = ("INSERT INTO TEMPERATURE "
                "(temp_date, collar_loc_id, temp_max, temp_00_00, temp_04_00, temp_08_00, temp_12_00, temp_16_00, temp_20_00) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")

add_error_entry = ("INSERT INTO DATA_INPUT_ERRORS "
                "(error_details, source_file_id, error_count) "
                "VALUES (%s, %s, %s)")

add_file_metadata = ("INSERT INTO FILE_METADATA "
                "(file_name, entry_date, file_type, file_size_kb, file_location, collar_id, import_status, import_percent) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)")

get_file_id_by_file_name_sql = ("SELECT file_id, import_status FROM FILE_METADATA WHERE FILE_NAME = %(file_name)s")

get_collar_allocation_by_cow_id = ("SELECT COLLAR_LOC_ID FROM COLLAR_ALLOCATION WHERE (COW_ID = %(cow_id)s AND date_off IS NULL)")

get_cow_by_cow_id_sql = ("SELECT COW_ID FROM COW WHERE COW_ID = %(cow_id)s")

get_collar_by_collar_id_sql = ("SELECT COLLAR_ID FROM COLLAR WHERE COLLAR_ID = %(collar_id)s")

get_collar_by_collar_num = ("SELECT COLLAR_ID FROM COLLAR WHERE COLLAR_NUM = %(collar_num)s")

get_cow_id_by_collar_id_sql = ("SELECT COW_ID FROM COLLAR_ALLOCATION WHERE (COLLAR_ID = %(collar_id)s AND date_off IS NULL)")

get_table = ("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = %(table_name)s")

update_file_import_status_sql = ("UPDATE file_metadata SET IMPORT_STATUS = %(import_status)s WHERE (`FILE_ID` = %(file_id)s);")

update_file_import_percent_status = ("UPDATE file_metadata SET IMPORT_PERCENT = %(import_percent)s WHERE (`FILE_NAME` = %(file_name)s);")

###############################################################
# Methods
###############################################################

def generate_activity_query(table_name):
    add_table = ("CREATE TABLE " + table_name + " "
        "(ACT_ID_FOUR_SEC BIGINT NOT NULL AUTO_INCREMENT,"
        "ACT_DATE_TIME DATETIME,"
        "COLLAR_LOC_ID BIGINT NOT NULL,"
        "ACT_STAT SMALLINT,"
        "ACT_REC SMALLINT,"
        "PRIMARY KEY (ACT_ID_FOUR_SEC ),"
        "FOREIGN KEY (COLLAR_LOC_ID) REFERENCES COLLAR_ALLOCATION(COLLAR_LOC_ID))")
    return add_table

def generate_location_query(table_name):
    add_table = ("CREATE TABLE " + table_name + " "
        "(LOC_ID_FIVE_MIN BIGINT NOT NULL AUTO_INCREMENT,"
        "LOC_DATE_TIME DATETIME,"
        "COLLAR_LOC_ID BIGINT NOT NULL,"
        "LOC_LAT FLOAT,"
        "LOC_LON FLOAT,"
        "LOC_REC MEDIUMINT,"
        "PRIMARY KEY (LOC_ID_FIVE_MIN ),"
        "FOREIGN KEY (COLLAR_LOC_ID) REFERENCES COLLAR_ALLOCATION(COLLAR_LOC_ID))")
    return add_table

def add_location_data(collar_id, db_cursor, db_cnx):
    collar_id = collar_id.replace("-", "_")
    table_name = "LOCATION_FIVE_MINS_" + collar_id + " "
    if check_table_exists(table_name, db_cursor) is None:
        create_location_table(table_name, db_cursor, db_cnx)
    add_location = ( "INSERT INTO " + table_name +
                "(LOC_DATE_TIME, COLLAR_LOC_ID, LOC_LAT, LOC_LON, LOC_REC) "
                "VALUES (%(LOC_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(LOC_LAT)s, %(LOC_LON)s, %(LOC_REC)s)")
    return add_location

def add_activity_data(collar_id, db_cursor, db_cnx):
    collar_id = collar_id.replace("-", "_")
    table_name = "ACTIVITY_FOUR_SECS_" + collar_id + " "
    if check_table_exists(table_name, db_cursor) is None:
        create_activity_table(table_name, db_cursor, db_cnx)
    add_activity = ( "INSERT INTO " + table_name + 
                "(ACT_DATE_TIME, COLLAR_LOC_ID, ACT_STAT, ACT_REC) "
                "VALUES (%(ACT_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(ACT_STAT)s, %(ACT_REC)s)")
    return add_activity

def check_table_exists(table_name, db_cursor):
    db_cursor.execute(get_table, { 'table_name' : table_name})
    row = db_cursor.fetchone()
    if row is not None:
        return row[2]
    else:
        return None

def create_activity_table(table_name, db_cursor, db_cnx):
    add_activity_table = generate_activity_query(table_name)
    db_cursor.execute(add_activity_table)
    db_cnx.commit()

def create_location_table(table_name, db_cursor, db_cnx):
    add_location_table = generate_location_query(table_name)
    db_cursor.execute(add_location_table)
    db_cnx.commit()

def setup_mysql_connection(user, password, host, database):
    cnx = mysql.connector.connect(user=user, password=password,host=host,database=database)
    return cnx

def import_activity_csv(file_name, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id):
    print("Importing activity file: % s" % file_name)
    with open(abs_path) as csv_file:
            csv_reader = pandas.read_csv(csv_file, sep=',', header=None, memory_map=True)
            line_counter = 0
            csv_row_counter= len(open(abs_path).readlines())
            one_percent_marker = int(csv_row_counter / 100)
            percent_line_counter = 0
            percent_counter = 0
            for index, row in csv_reader.iterrows():
                print("Progress: " + str(line_counter + 1) + "/" + str(csv_row_counter), flush=True)
                time_stamp = row[0]
                record_id = process_record_row(row)
                activity = process_activity(str(row[1]))
                insert_activity_data(db_cursor, db_cnx, collar_allocation_id, time_stamp, activity, record_id, collar_id)
                line_counter += 1
                percent_line_counter += 1
                if(percent_line_counter == one_percent_marker):
                    percent_counter += 1
                    update_import_status(file_name, db_cursor, db_cnx, percent_counter)
                    percent_line_counter = 0
                if line_counter == 1:
                    continue


def update_import_status(file_name, db_cursor, db_cnx, current_import_percent):
    current_import_percent = current_import_percent
    print("One percent complete")
    db_cursor.execute(update_file_import_percent_status, { 'import_percent' : current_import_percent, 'file_name' : file_name})
    db_cnx.commit()

def process_activity(record):
    if "*" in record:
        return None
    else:
        return record


def insert_activity_data(db_cursor, db_cnx,collar_allocation_id, time_date_details, activity, record_id, collar_id):
    add_activity = add_activity_data(collar_id, db_cursor, db_cnx)
    activity_data = {
                    'ACT_DATE_TIME' : time_date_details,
                    'COLLAR_LOC_ID' : collar_allocation_id,
                    'ACT_STAT' : activity,
                    'ACT_REC' : record_id
                }
    db_cursor.execute(add_activity, activity_data)


def import_daily_csv(file_name, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id):
    print("Importing daily file: % s" % file_name)
    with open(abs_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_counter = 0
            csv_row_counter= len(open(abs_path).readlines())
            for row in csv_reader:
                if line_counter == 0:
                    line_counter += 1
                else:
                    print("Progress: " + str(line_counter + 1) + "/" + str(csv_row_counter), flush=True)
                    time_stamp = row[0]
                    daily_activity_readings = (time_stamp, collar_allocation_id, row[2], row[3], row[4], row[5])
                    lat_and_long_data = get_lat_long_data(row[6], row[7], row[8])
                    daily_location_readings = (time_stamp, collar_allocation_id, lat_and_long_data[0][0], lat_and_long_data[0][1], lat_and_long_data[1][0], lat_and_long_data[1][1], lat_and_long_data[2][0], lat_and_long_data[2][1])
                    daily_battery_readings = (time_stamp, collar_allocation_id, row[10], row[11], row[19], row[20], row[21], row[22], row[23], row[24])
                    daily_temp_readings = (time_stamp, collar_allocation_id, row[9], row[13], row[14], row[15], row[16], row[17], row[18])
                    daily_status = (time_stamp, collar_allocation_id, row[12])

                    db_cursor.execute(add_daily_activity, daily_activity_readings)
                    db_cursor.execute(add_daily_location, daily_location_readings)
                    db_cursor.execute(add_battery, daily_battery_readings)
                    db_cursor.execute(add_temperature, daily_temp_readings)
                    db_cursor.execute(add_status, daily_status)
                    line_counter += 1


def get_lat_long_data(lat_long1, lat_long2, lat_long3):
    location_data = [[],[],[]]
    location_data[0] = lat_long1.split()
    location_data[1] = lat_long2.split()
    location_data[2] = lat_long3.split()
    return location_data


def import_location_csv(file_name, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id, file_id):
    print("Importing location file: % s" % file_name)
    with open(abs_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        error_logging = {
            "* in location": 0,
            "null location": 0,
            "---- in location": 0
        }
        line_counter = 0
        csv_row_counter= len(open(abs_path).readlines())
        one_percent_marker = int(csv_row_counter / 100)
        percent_line_counter = 0
        percent_counter = 0
        for row in csv_reader:
            print("Progress: " + str(line_counter + 1) + "/" + str(csv_row_counter), flush=True)
            time_stamp = row[0]
            location_data, error_logging = process_location_lat_long(row[1], error_logging)
            record_id = process_record_row(row)
            insert_location_data(db_cursor, db_cnx, collar_allocation_id, time_stamp, location_data, record_id, collar_id)
            line_counter += 1
            percent_line_counter += 1
            if(percent_line_counter == one_percent_marker):
                percent_counter += 1
                update_import_status(file_name, db_cursor, db_cnx, percent_counter)
                percent_line_counter = 0
            if line_counter == 1:
                continue


def check_file_exists(file_name, abs_path, db_cursor, db_cnx, collar_id):
    file_id = get_file_id_by_file_name(file_name, db_cursor)
    if not file_id:
        create_file_id(file_name, abs_path, db_cursor, db_cnx, collar_id)
        file_id = get_file_id_by_file_name(file_name, db_cursor)
    elif(file_id[1] == 0):
        print(file_name + " already imported")
        exit()
    return file_id[0]

def get_file_id_by_file_name(file_name, db_cursor):
    db_cursor.execute(get_file_id_by_file_name_sql, { 'file_name' : file_name})
    row = db_cursor.fetchone()
    if row is not None:
        return row
    else:
        return None

def create_file_id(file_name, abs_path, db_cursor, db_cnx, collar_id):
    today = datetime.today()
    file_size = os.path.getsize(abs_path)
    file_details = (file_name, today, "csv", file_size, abs_path, collar_id, True, 0)
    db_cursor.execute(add_file_metadata, file_details)
    db_cnx.commit()

def insert_error_data(db_cursor, file_id, error_logging):
    print(error_logging)
    for key in error_logging:
        error_data = (key, file_id, error_logging[key])
        db_cursor.execute(add_error_entry, error_data)


def process_location_lat_long(lat_long, error_logging):
    if "*" in lat_long:
        error_logging["* in location"] += 1
        processed_lat_long = [None, None]
    elif "---------" in lat_long:
        error_logging["---- in location"] += 1
        processed_lat_long = [None, None]
    elif lat_long == "":
        error_logging["null location"] += 1
        processed_lat_long = [None, None]
    else:
        processed_lat_long = lat_long.split()
    return processed_lat_long, error_logging


def process_record_row(record):
    if "*" in record[1]:
        return None
    else:
        return record[2].split()[1]


def insert_location_data(db_cursor, db_cnx, collar_allocation_id, time_date_details, location_data, record_id, collar_id):
    add_location = add_location_data(collar_id, db_cursor, db_cnx)
    location_data = {
                    'LOC_DATE_TIME' : time_date_details,
                    'COLLAR_LOC_ID' : collar_allocation_id,
                    'LOC_LAT' : location_data[0],
                    'LOC_LON' : location_data[1],
                    'LOC_REC' : record_id
                }
    db_cursor.execute(add_location, location_data)

def check_cow_exists(collar_id, db_cnx, db_cursor):
    cow_id = get_cow_id_by_collar_id(collar_id, db_cursor, db_cnx)
    cow = get_cow_by_cow_id(cow_id, db_cursor)
    if not cow:
        create_cow(cow_id, db_cnx, db_cursor)

def get_cow_id_by_collar_id(collar_id, db_cursor, db_cnx):
    db_cursor.execute(get_cow_id_by_collar_id_sql, { 'collar_id' : collar_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None


def generate_temp_cow_id(collar_id, db_cursor, db_cnx):
    random_id = random.randint(1,100000)
    temp_id = 'temp_' + str(random_id)
    create_cow(temp_id, db_cnx, db_cursor)
    return temp_id


def get_cow_by_cow_id(cow_id, db_cursor):
    db_cursor.execute(get_cow_by_cow_id_sql, { 'cow_id' : cow_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None


def create_cow(cow_id, db_cnx, db_cursor):
    cow_details = (cow_id, None, None, None)
    db_cursor.execute(add_cow, cow_details)
    db_cnx.commit()


def check_collar_exists(collar_id, collar_num, db_cnx, db_cursor):
    collar = get_collar_by_collar_id(collar_id, db_cursor)
    if not collar:
        create_collar(collar_id, collar_num, db_cnx, db_cursor)


def get_collar_by_collar_id(collar_id, db_cursor):
    db_cursor.execute(get_collar_by_collar_id_sql, { 'collar_id' : collar_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None


def create_collar(collar_id, collar_num, db_cnx, db_cursor):
    collar_details = (collar_id, None, collar_num, None)
    db_cursor.execute(add_collar, collar_details)
    db_cnx.commit()


def get_collar_allocation(collar_id, cow_id, db_cnx, db_cursor):
    collar_allocation_id = check_collar_allocation_id_by_cow_id(cow_id, db_cursor)
    if not collar_allocation_id:
        collar_allocation_id = generate_collar_allocation(cow_id, collar_id, db_cnx, db_cursor)
        return collar_allocation_id
    return collar_allocation_id


def check_collar_allocation_id_by_cow_id(cow_id, db_cursor):
    db_cursor.execute(get_collar_allocation_by_cow_id, { 'cow_id' : cow_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None


def get_collar_id_from_csv(parent_folder):
    for item in os.listdir(parent_folder):
        if item.find('Daily') != -1:
            abs_path = parent_folder + "/" + item
            with open(abs_path) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                row_counter = 0
                for row in csv_reader:
                    if row_counter == 1:
                        collar_id = row[1]
                        break
                    row_counter += 1
            csv_file.close()
            collar_id = collar_id.strip()
            return collar_id


def generate_collar_allocation(cow_id, collar_id, db_cnx, db_cursor):
    collar_allocation_details = (cow_id, collar_id, None, None)
    db_cursor.execute(add_collar_allocation, collar_allocation_details)
    db_cnx.commit()
    collar_allocation_id = check_collar_allocation_id_by_cow_id(cow_id, db_cursor)
    if collar_allocation_id is None:
        raise Exception('Data mismatch error, unable to retrive the generated collar allocation ID from the database')
    return collar_allocation_id

def split_file_name(full_file_path):
    file_name = full_file_path.split('\\')
    if(file_name is None):
        file_name = full_file_path.split('/')
    file_name = file_name[-1]
    file_name_split = file_name.split("-")
    file_type = file_name_split[0]
    collar_num = file_name_split[1].strip()
    return file_type, collar_num, file_name


def get_collar_id_from_collar_num(collar_num, db_cnx, db_cursor):
    db_cursor.execute(get_collar_by_collar_num, { 'collar_num' : collar_num})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None

def update_file_import_status(file_id, db_cnx, db_cursor):
    db_cursor.execute(update_file_import_status_sql, { 'import_status' : False, 'file_id' : file_id})
    db_cnx.commit()

###############################################################
# Main Method
###############################################################

def main(file_name_arg):
    start = time.time()
    db_cnx = setup_mysql_connection(SQL_SERVICE_USER, SQL_SERVICE_PASSWORD, SQL_HOST, SQL_DATABASE)
    db_cursor = db_cnx.cursor(buffered=True)
    file_type, collar_num, file_name = split_file_name(file_name_arg)
    collar_id = get_collar_id_from_collar_num(collar_num, db_cnx, db_cursor)
    cow_id = get_cow_id_by_collar_id(collar_id, db_cursor, db_cnx)
    if(collar_id is None):
        print("Unable to find a collar ID for the collar number: " + collar_num)
        exit()
    if(cow_id is None):
        print("No cow has currently been allocated to the collar: " + collar_num)
        exit()
    collar_allocation_id = get_collar_allocation(collar_id, cow_id, db_cnx, db_cursor)
    if(file_type == "activity4s"):
        print("Importing Acitivity file")
        file_id = check_file_exists(file_name, file_name_arg, db_cursor, db_cnx, collar_id)
        import_activity_csv(file_name, file_name_arg, db_cursor, db_cnx, collar_allocation_id, collar_id)
    elif(file_type == "Daily"):
        file_id = check_file_exists(file_name, file_name_arg, db_cursor, db_cnx, collar_id)
        print("Importing Daily file")
        import_daily_csv(file_name, file_name_arg, db_cursor, db_cnx, collar_allocation_id, collar_id)
    elif(file_type == "location5min"):
        file_id = check_file_exists(file_name, file_name_arg, db_cursor, db_cnx, collar_id)
        print("Importing Location file")
        import_location_csv(file_name, file_name_arg, db_cursor, db_cnx, collar_allocation_id, collar_id, file_id)
    else:
        print("ERROR: Unable to determine the file type. This file will not be imported")
        exit()
    if(file_id is not None):
        print("Updating file import status for " + str(file_id))
        print(file_name + " Import Complete")
        update_file_import_status(file_id, db_cnx, db_cursor)
    db_cursor.close()
    db_cnx.commit()
    db_cnx.close()
    end = time.time()
    print("Import Time: " + str(end - start))

if __name__ == '__main__':
    main(sys.argv[1])
import csv
import os
import mysql.connector
import random
from zipfile import ZipFile
from datetime import datetime

# TODO
# On the front end a search can be performed to find any cows with temp ids and alert the user that these need to updated for data
# to display correctly

# TODO
# Implement the following to speed up the import by importing all data from a CSV into a list then processing the list
# https://stackoverflow.com/questions/36587211/easiest-way-to-read-csv-files-with-multiprocessing-in-pandas/46345838

# TODO
# Create consistanency between the nameing convention of the functions

# SQL CONSTANTS
SQL_SERVICE_USER = 'root'
SQL_SERVICE_PASSWORD = ''
SQL_HOST = '127.0.0.1'
SQL_DATABASE = 'smartFarm'

# SQL QUERIES
add_cow = ("INSERT INTO COW "
                "(cow_id, date_of_birth, date_of_entry, date_of_exit) "
                "VALUES (%s, %s, %s, %s)")

add_collar = ("INSERT INTO COLLAR "
                "(collar_id, collar_ser_no, collar_num, date_of_entry) "
                "VALUES (%s, %s, %s, %s)")

add_collar_allocation = ("INSERT INTO COLLAR_ALLOCATION "
                "(cow_id, collar_id, date_on, date_off) "
                "VALUES (%s, %s, %s, %s)")
                
add_daily_activity = ("INSERT INTO ACTIVITY_DAILY "
                "(act_date, collar_loc_id, act_rest, act_walk, act_graze, act_bout) "
                "VALUES (%s, %s, %s, %s, %s, %s)")

add_daily_location = ("INSERT INTO LOCATION_DAILY "
                "(loc_date, collar_loc_id, loc_1_lat, loc_1_long, loc_2_lat, loc_2_long, loc_3_lat, loc_3_long) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)")

add_battery = ("INSERT INTO BATTERY "
                "(batt_date, collar_loc_id, batt_min, batt_max, batt_00_00, batt_04_00, batt_08_00, batt_12_00, batt_16_00, batt_20_00) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")

add_status = ("INSERT INTO STATUS "
                "(stat_date, collar_loc_id, stat_sys) "
                "VALUES (%s, %s, %s)")

add_temperature = ("INSERT INTO TEMPERATURE "
                "(temp_date, collar_loc_id, temp_max, temp_00_00, temp_04_00, temp_08_00, temp_12_00, temp_16_00, temp_20_00) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")

add_error_entry = ("INSERT INTO DATA_INPUT_ERRORS "
                "(error_details, source_file_id, error_count) "
                "VALUES (%s, %s, %s)")

add_file_metadata = ("INSERT INTO FILE_METADATA "
                "(file_name, entry_date, file_type, file_size_kb, file_location, collar_id) "
                "VALUES (%s, %s, %s, %s, %s, %s)")

get_file_id_by_file_name = ("SELECT FILE_ID FROM FILE_METADATA WHERE FILE_NAME = %(file_name)s")

get_collar_allocation_by_cow_id = ("SELECT COLLAR_LOC_ID FROM COLLAR_ALLOCATION WHERE (COW_ID = %(cow_id)s AND date_off IS NULL)")

get_cow_by_cow_id = ("SELECT COW_ID FROM COW WHERE COW_ID = %(cow_id)s")

get_collar_by_collar_id = ("SELECT COLLAR_ID FROM COLLAR WHERE COLLAR_ID = %(collar_id)s")

get_cow_id_by_collar_id = ("SELECT COW_ID FROM COLLAR_ALLOCATION WHERE COLLAR_ID = %(collar_id)s")

get_table = ("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = %(table_name)s")

def generateActivityQuery(table_name):
    add_table = ("CREATE TABLE " + table_name + " "
        "(ACT_ID_FOUR_SEC BIGINT NOT NULL AUTO_INCREMENT,"
        "ACT_DATE_TIME DATETIME,"
        "COLLAR_LOC_ID BIGINT NOT NULL,"
        "ACT_STAT SMALLINT,"
        "ACT_REC SMALLINT,"
        "PRIMARY KEY (ACT_ID_FOUR_SEC ),"
        "FOREIGN KEY (COLLAR_LOC_ID) REFERENCES COLLAR_ALLOCATION(COLLAR_LOC_ID))")
    return add_table

def generateLocationQuery(table_name):
    add_table = ("CREATE TABLE " + table_name + " "
        "(LOC_ID_FIVE_MIN BIGINT NOT NULL AUTO_INCREMENT,"
        "LOC_DATE_TIME DATETIME,"
        "COLLAR_LOC_ID BIGINT NOT NULL,"
        "LOC_LAT FLOAT,"
        "LOC_LON FLOAT,"
        "LOC_REC MEDIUMINT,"
        "PRIMARY KEY (LOC_ID_FIVE_MIN ),"
        "FOREIGN KEY (COLLAR_LOC_ID) REFERENCES COLLAR_ALLOCATION(COLLAR_LOC_ID))")
    return add_table

def addLocationData(collar_id, db_cursor, db_cnx):
    today = datetime.today()
    date = today.strftime("%b%Y")
    collar_id = collar_id.replace("-", "_")
    table_name = "LOCATION_FIVE_MINS_" + date + "_" + collar_id + " "
    if checkTableExists(table_name, db_cursor) is None:
        createLocationTable(table_name, db_cursor, db_cnx)
    add_location = ( "INSERT INTO " + table_name +
                "(LOC_DATE_TIME, COLLAR_LOC_ID, LOC_LAT, LOC_LON, LOC_REC) "
                "VALUES (%(LOC_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(LOC_LAT)s, %(LOC_LON)s, %(LOC_REC)s)")
    return add_location

def addActivityData(collar_id, db_cursor, db_cnx):
    today = datetime.today()
    date = today.strftime("%b%Y")
    collar_id = collar_id.replace("-", "_")
    table_name = "ACTIVITY_FOUR_SECS_" + date + "_" + collar_id + " "
    if checkTableExists(table_name, db_cursor) is None:
        createActivityTable(table_name, db_cursor, db_cnx)
    add_activity = ( "INSERT INTO " + table_name + 
                "(ACT_DATE_TIME, COLLAR_LOC_ID, ACT_STAT, ACT_REC) "
                "VALUES (%(ACT_DATE_TIME)s, %(COLLAR_LOC_ID)s, %(ACT_STAT)s, %(ACT_REC)s)")
    return add_activity

def checkTableExists(table_name, db_cursor):
    db_cursor.execute(get_table, { 'table_name' : table_name})
    row = db_cursor.fetchone()
    if row is not None:
        return row[2]
    else:
        return None

def createActivityTable(table_name, db_cursor, db_cnx):
    add_activity_table = generateActivityQuery(table_name)
    db_cursor.execute(add_activity_table)
    db_cnx.commit()

def createLocationTable(table_name, db_cursor, db_cnx):
    add_location_table = generateLocationQuery(table_name)
    db_cursor.execute(add_location_table)
    db_cnx.commit()

def setupMySQLConnection(user, password, host, database):
    cnx = mysql.connector.connect(user=user, password=password,host=host,database=database)
    return cnx


def extractFoldersFromZips(parent_folder):
    for item in os.listdir(parent_folder):
        if item.endswith(".zip"):
            file_name = parent_folder + "/" + item
            zip_to_folder_name = file_name.strip('.zip')
            with ZipFile(file_name) as zipObj:
                zipObj.extractall(parent_folder)
                print('% s unzipped and renamed % s'%(item, zip_to_folder_name))


def importActivityCsv(file_name, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id):
    print("Importing activity file: % s" % file_name)
    file_id = checkFileExists(file_name, abs_path, db_cursor, db_cnx, collar_id)
    with open(abs_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                print(row)
                time_stamp = row[0]
                record_id = processRecordRow(row)
                activity = processActivity(row[1])
                insertActivityData(db_cursor, db_cnx, collar_allocation_id, time_stamp, activity, record_id, collar_id)


def processActivity(record):
    if "*" in record:
        return None
    else:
        return record


def insertActivityData(db_cursor, db_cnx,collar_allocation_id, time_date_details, activity, record_id, collar_id):
    add_activity = addActivityData(collar_id, db_cursor, db_cnx)
    activity_data = {
                    'ACT_DATE_TIME' : time_date_details,
                    'COLLAR_LOC_ID' : collar_allocation_id,
                    'ACT_STAT' : activity,
                    'ACT_REC' : record_id
                }
    db_cursor.execute(add_activity, activity_data)


def importDailyCsv(file_name, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id):
    print("Importing daily file: % s" % file_name)
    file_id = checkFileExists(file_name, abs_path, db_cursor, db_cnx, collar_id)
    with open(abs_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    print(row)
                    time_stamp = row[0]
                    daily_activity_readings = (time_stamp, collar_allocation_id, row[2], row[3], row[4], row[5])
                    lat_and_long_data = getLatLongData(row[6], row[7], row[8])
                    daily_location_readings = (time_stamp, collar_allocation_id, lat_and_long_data[0][0], lat_and_long_data[0][1], lat_and_long_data[1][0], lat_and_long_data[1][1], lat_and_long_data[2][0], lat_and_long_data[2][1])
                    daily_battery_readings = (time_stamp, collar_allocation_id, row[10], row[11], row[19], row[20], row[21], row[22], row[23], row[24])
                    daily_temp_readings = (time_stamp, collar_allocation_id, row[9], row[13], row[14], row[15], row[16], row[17], row[18])
                    daily_status = (time_stamp, collar_allocation_id, row[12])

                    db_cursor.execute(add_daily_activity, daily_activity_readings)
                    db_cursor.execute(add_daily_location, daily_location_readings)
                    db_cursor.execute(add_battery, daily_battery_readings)
                    db_cursor.execute(add_temperature, daily_temp_readings)
                    db_cursor.execute(add_status, daily_status)
                    line_count += 1


def getLatLongData(lat_long1, lat_long2, lat_long3):
    location_data = [[],[],[]]
    location_data[0] = lat_long1.split()
    location_data[1] = lat_long2.split()
    location_data[2] = lat_long3.split()
    return location_data


def importLocationCsv(file_name, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id):
    print("Importing location file: % s" % file_name)
    file_id = checkFileExists(file_name, abs_path, db_cursor, db_cnx, collar_id)
    with open(abs_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        error_logging = {
            "* in location": 0,
            "null location": 0,
            "---- in location": 0
        }
        for row in csv_reader:
            print(row)
            time_stamp = row[0]
            location_data, error_logging = processLocationLatLong(row[1], error_logging)
            record_id = processRecordRow(row)
            insertLocationData(db_cursor, db_cnx, collar_allocation_id, time_stamp, location_data, record_id, collar_id)
        
        if(error_logging["* in location"] > 0 or error_logging["null location"] > 0 or error_logging["---- in location"] > 0):
            print(file_id)
            insertErrorData(db_cursor, file_id, error_logging)


def checkFileExists(file_name, abs_path, db_cursor, db_cnx, collar_id):
    file_id = getFileIdByFileName(file_name, db_cursor)
    if not file_id:
        createFileId(file_name, abs_path, db_cursor, db_cnx, collar_id)
        file_id = getFileIdByFileName(file_name, db_cursor)
    return file_id

def getFileIdByFileName(file_name, db_cursor):
    db_cursor.execute(get_file_id_by_file_name, { 'file_name' : file_name})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None

def createFileId(file_name, abs_path, db_cursor, db_cnx, collar_id):
    today = datetime.today()
    file_size = os.path.getsize(abs_path)
    file_details = (file_name, today, "csv", file_size, abs_path, collar_id)
    db_cursor.execute(add_file_metadata, file_details)
    db_cnx.commit()

def insertErrorData(db_cursor, file_id, error_logging):
    print(error_logging)
    for key in error_logging:
        error_data = (key, file_id, error_logging[key])
        db_cursor.execute(add_error_entry, error_data)


def processLocationLatLong(lat_long, error_logging):
    if "*" in lat_long:
        error_logging["* in location"] += 1
        processed_lat_long = [None, None]
    elif "---------" in lat_long:
        error_logging["---- in location"] += 1
        processed_lat_long = [None, None]
    elif lat_long == "":
        error_logging["null location"] += 1
        processed_lat_long = [None, None]
    else:
        processed_lat_long = lat_long.split()
    return processed_lat_long, error_logging


def processRecordRow(record):
    if len(record) == 2:
        return None
    else:
        return record[2].split()[1]


def insertLocationData(db_cursor, db_cnx, collar_allocation_id, time_date_details, location_data, record_id, collar_id):
    add_location = addLocationData(collar_id, db_cursor, db_cnx)
    location_data = {
                    'LOC_DATE_TIME' : time_date_details,
                    'COLLAR_LOC_ID' : collar_allocation_id,
                    'LOC_LAT' : location_data[0],
                    'LOC_LON' : location_data[1],
                    'LOC_REC' : record_id
                }
    db_cursor.execute(add_location, location_data)


def importDataFolder(folder_name, collar_num, db_cnx):
    db_cursor = db_cnx.cursor()
    collar_id = getCollarIdFromCsv(folder_name)
    checkCollarExists(collar_id, collar_num, db_cnx, db_cursor)
    cow_id = getCowIdByCollarId(collar_id, db_cursor, db_cnx)
    collar_allocation_id = getCollarAllocation(collar_id, cow_id, db_cnx, db_cursor)
    print("Importing data folder: % s with cowID: % s \tCollar Allocation ID: % s" % (folder_name, cow_id, collar_allocation_id))
    for item in os.listdir(folder_name):
        abs_path = folder_name + "/" + item
        if item.find('activity') != -1:
            importActivityCsv(item, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id)
            print("Acitivity")
        elif item.find('Daily') != -1:
            print("Daily")
            importDailyCsv(item, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id)
        elif item.find('location') != -1:
            print("Location")
            importLocationCsv(item, abs_path, db_cursor, db_cnx, collar_allocation_id, collar_id)


def checkCowExists(collar_id, db_cnx, db_cursor):
    cow_id = getCowIdByCollarId(collar_id, db_cursor, db_cnx)
    cow = getCowByCowId(cow_id, db_cursor)
    if not cow:
        createCow(cow_id, db_cnx, db_cursor)

def getCowIdByCollarId(collar_id, db_cursor, db_cnx):
    db_cursor.execute(get_cow_id_by_collar_id, { 'collar_id' : collar_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return generateTempCowId(collar_id, db_cursor, db_cnx)


def generateTempCowId(collar_id, db_cursor, db_cnx):
    random_id = random.randint(1,100000)
    temp_id = 'temp_' + str(random_id)
    createCow(temp_id, db_cnx, db_cursor)
    return temp_id


def getCowByCowId(cow_id, db_cursor):
    db_cursor.execute(get_cow_by_cow_id, { 'cow_id' : cow_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None


def createCow(cow_id, db_cnx, db_cursor):
    cow_details = (cow_id, None, None, None)
    db_cursor.execute(add_cow, cow_details)
    db_cnx.commit()


def checkCollarExists(collar_id, collar_num, db_cnx, db_cursor):
    collar = getCollarByCollarId(collar_id, db_cursor)
    if not collar:
        createCollar(collar_id, collar_num, db_cnx, db_cursor)


def getCollarByCollarId(collar_id, db_cursor):
    db_cursor.execute(get_collar_by_collar_id, { 'collar_id' : collar_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None


def createCollar(collar_id, collar_num, db_cnx, db_cursor):
    collar_details = (collar_id, None, collar_num, None)
    db_cursor.execute(add_collar, collar_details)
    db_cnx.commit()


def getCollarAllocation(collar_id, cow_id, db_cnx, db_cursor):
    collar_allocation_id = checkCollarAllocationIdByCowId(cow_id, db_cursor)
    if not collar_allocation_id:
        collar_allocation_id = generateCollarAllocation(cow_id, collar_id, db_cnx, db_cursor)
        return collar_allocation_id
    return collar_allocation_id


def checkCollarAllocationIdByCowId(cow_id, db_cursor):
    db_cursor.execute(get_collar_allocation_by_cow_id, { 'cow_id' : cow_id})
    row = db_cursor.fetchone()
    if row is not None:
        return row[0]
    else:
        return None


def getCollarIdFromCsv(parent_folder):
    for item in os.listdir(parent_folder):
        if item.find('Daily') != -1:
            abs_path = parent_folder + "/" + item
            with open(abs_path) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                row_counter = 0
                for row in csv_reader:
                    if row_counter == 1:
                        collar_id = row[1]
                        break
                    row_counter += 1
            csv_file.close()
            collar_id = collar_id.strip()
            return collar_id


def generateCollarAllocation(cow_id, collar_id, db_cnx, db_cursor):
    collar_allocation_details = (cow_id, collar_id, None, None)
    db_cursor.execute(add_collar_allocation, collar_allocation_details)
    db_cnx.commit()
    collar_allocation_id = checkCollarAllocationIdByCowId(cow_id, db_cursor)
    if collar_allocation_id is None:
        raise Exception('Data mismatch error, unable to retrive the generated collar allocation ID from the database')
    return collar_allocation_id


def processDataFolders(parent_folder, db_cnx):
    for item in os.listdir(parent_folder):
        # Put in to ignore OSX file system directory
        if item == '.DS_Store':
            continue

        file_name = parent_folder + "/" + item
        if os.path.isdir(file_name):
            importDataFolder(file_name, item, db_cnx)


def main():
    cnx = setupMySQLConnection(SQL_SERVICE_USER, SQL_SERVICE_PASSWORD, SQL_HOST, SQL_DATABASE)
    db_cursor = cnx.cursor()
    data_directory = input("Please enter the location of the data folders:")
    extractFoldersFromZips(data_directory)
    processDataFolders(data_directory, cnx)
    db_cursor.close()
    cnx.commit()
    cnx.close()


if __name__ == '__main__':
    main()
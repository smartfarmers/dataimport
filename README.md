# README #
### FILE & FOLDER LEGEND ###

Folders:
* SampleData: Contains sample data to be used with manually testing. Is just a cut down version of one of the extracts provided by Jon
* TestData: Is used by the integration tests to fill the test database with with testdata, if this is changed. The Int tests will need to be updated as they expect certain data to exist

Files:
* CleanImportDataIntoDB_Int_test.py: Contains the Integration tests for the data import script
* CleanImportDataIntoDB_Unit_test.py: Contains the Unit tests for the data import script
* CleanImportDataIntoDB.py: Contains the main script for the data import
* pytest_helper_functions.py: Contains helper functions for the test scripts
* TestPythonSQL.py: Contains SQL commands wrapped in python objects to be used in setting up the test DB for the Integration testing
* TestPythonSQL.sql: Contains an SQL script to be used to setup the test database for Integration testing

### What is this repository for? ###

* Quick summary
This repo contains the python scripts that will be used to import the CSV files with the cow data directly into the database.

* Version

### How do I get set up? ###

* Summary of set up
1. Install dependencies

* Configuration


* Dependencies
  1. mysql-connector

* How to run tests
 1. Run pip install pytest
 2. Setup up test MySQL database by running the sql script "TestPythongSQL.sql"
 3. In the main folder run `pytest --pyargs`
   - To view all the tests being ran, run `pytest --pyargs -r p`

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review